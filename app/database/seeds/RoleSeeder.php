<?php

// class used to seed the roles table with data
class RoleSeeder extends Seeder {
	
	/**
	 * Run the roles seeds.
	 *
	 * @return void
	 */
	public function run() {
		// DB::table ( 'roles' )->delete ();
		
		Role::create ( array (
				'id' => '1',
				'name' => 'member' 
		) );
		
		Role::create ( array (
				'id' => '2',
				'name' => 'librarian' 
		) );
		
		Role::create ( array (
				'id' => '3',
				'name' => 'administrator' 
		) );
		
		Role::create ( array (
				'id' => '4',
				'name' => 'super_administrator' 
		) );
	}
}