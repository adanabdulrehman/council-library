<?php

// class used to seed the globalSettings table with data
class GlobalSettingsSeeder extends Seeder {
	
	/**
	 * Run the global settings seeds.
	 *
	 * @return void
	 */
	public function run() {
		
		DB::table ( 'globalsettings' )->insert ( array (
				array (
						'id' => '1',
						'permissable_loan_period' => '14',
						'global_book_allowance_under1yr' => '4',
						'global_book_allowance_over1yr' => '6',
						'loan_rate' => '5',
						'show_most_borrowed' => '5',
						'show_highest_rated' => '5',
						'created_at' => new DateTime,
						'updated_at' => new DateTime,
				) 
		) );
	}
}