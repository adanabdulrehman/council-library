<?php
class DatabaseSeeder extends Seeder {
	
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		Eloquent::unguard ();

		
		DB::table ( 'roles' )->delete ();
		DB::table ( 'users' )->delete ();
		DB::table ( 'users_roles' )->delete ();
		DB::table ( 'loans' )->delete ();
		DB::table ( 'reviews' )->delete ();
		DB::table ( 'books' )->delete ();
		DB::table ( 'genres' )->delete ();
		DB::table ( 'authors' )->delete ();
		DB::table ( 'globalsettings' )->delete ();
		
		// added a role seed for test purposes
		$this->call ( 'RoleSeeder' );
		
		// added a user seed for test purposes
		$this->call ( 'UserSeeder' );
		
		// added a user role seed for test purposes
		$this->call ( 'UserRoleSeeder' );
		
		// added a genre seed for test purposes
		$this->call ( 'GenreSeeder' );
		
		// added a author seed for test purposes
		$this->call ( 'AuthorSeeder' );
		
		// added a book seed for test purposes
		$this->call ( 'BookSeeder' );
		
		// added a loan seed for test purposes
		$this->call ( 'LoanSeeder' ); 
		
		// added a global Settings seed for test purposes
		$this->call ( 'GlobalSettingsSeeder' );
	}
}