<?php

// This is the controller for user login, registration
class UsersController extends BaseController {
	public function __construct() {
		
		// add the filters for csrf to avoid the attack
		// the reason is that we are going to recieve input
		// and we'd like to verify where it is coming from
		$this->beforeFilter ( 'csrf', array (
				'on' => 'post' 
		) );
	}
	
	// set the $layout property to the login layout as main.blade
	protected $layout = "layouts.login.main";
	
	// action for login a user
	public function getLogin() {
		$this->layout->content = View::make ( 'users.login' );
	}
	
	// action for handling user logout
	public function getLogout() {
		
		// only logout if logged in
		if (Auth::check ()) {
			Auth::logout ();
		}
		return Redirect::to ( 'users/login' );
	}
	
	// action for handling user signin
	public function signin() {
		if (Auth::attempt ( array (
				'email' => Input::get ( 'email' ),
				'password' => Input::get ( 'password' ) 
		) )) {
			
			$user = Auth::user ();
			
			if ($user->hasRole ( 'super_administrator' )) {
				return Redirect::to ( 'administrator' );
			} else if ($user->hasRole ( 'administrator' )) {
				return Redirect::to ( 'administrator' );
			} else if ($user->hasRole ( 'librarian' )) {
				return Redirect::to ( 'librarian' );
			} else if ($user->hasRole ( 'member' )) {
				return Redirect::to ( 'member' );
			} else {
				return Redirect::to ( 'users/login' )->with ( 'message', 'Your username/password combination was incorrect' )->withInput ();
			}
		} else {
			
			return Redirect::to ( 'users/login' )->with ( 'message', 'Your username/password combination was incorrect' )->withInput ();
		}
	}
	
	// action for registering a user
	public function getRegister() {
		
		// set the content layout property.
		// This is the $content variable in the login layout file
		// to display a users.register view file.
		$this->layout->content = View::make ( 'users.register' );
	}
	
	// action for handling processing the form submission by
	// validating the data and either displaying validation error
	// messages or it creates the new user, hashing the user's
	// password, and saving the user into the database
	public function postCreate() {
		$validator = Validator::make ( Input::all (), User::$rules );
		
		if ($validator->passes ()) {
			// validation has passed, save user in DB
			$user = new User ();
			$user->username = Input::get ( 'username' );
			$user->email = Input::get ( 'email' );
			$user->password = Hash::make ( Input::get ( 'password' ) );
			$user->firstname = Input::get ( 'firstname' );
			$user->secondname = Input::get ( 'secondname' );
			$user->address = Input::get ( 'address' );
			$user->city = Input::get ( 'city' );
			$user->phone = Input::get ( 'phone' );
			$user->phone = 'pending';
			$user->save ();
			
			$insertedId = $user->id;
			DB::table ( 'users_roles' )->insert (array ('user_id' => $insertedId, 'role_id' => '1' ));

			$user->makeLibraryUser ( 'member' );
			$user->push();
			return Redirect::to ( 'users/login' )->with ( 'message', 'Thank you for registering, you will be contact in the next 24 hours informing you of status' );
		} else {
			return Redirect::to ( 'users/register' )->with ( 'message', 'The following errors occurred' )->withErrors ( $validator )->withInput ();
		}
	}
}

?>