<?php

use Carbon\Carbon;

// using the registered service for getting global settings
// for the application such as yearly bookAllowance, loan period etc
use settings\service\GlobalSettingService;
use settings\GlobalSetting;

// This is the default homepage calling the view home
class HomeController extends BaseController {
	 
	// Settings service to query the settings table
	private $GlobalSettingService;
	
	function __construct(GlobalSettingService $GlobalSettingService) {
		
		// inject the service
		$this->GlobalSettingService = $GlobalSettingService;
	
		// protecting any post actions from csrf attacks
		$this->beforeFilter ( 'csrf', array (
				'on' => 'post'
		) );
	}
	
	// Default Home Controller
	public function showWelcome() {

		$showMostBorrowedValue = $this->GlobalSettingService->getSettings()->getShowMostBorrowed ();
		$showHighestRatedValue = $this->GlobalSettingService->getSettings()->getShowHighestRated ();
	
		// the highest rated books
		$reviews = Review::select('book_id',DB::raw('avg(rating) as averageScore'))->groupBy('book_id')->orderBy('rating')->take($showHighestRatedValue)->get();
		$reviews = $reviews->sortBy('averageScore')->reverse();
		
		// the most borrowed books
		
		// using Carbon api for dates !!!
		$yearAgo = Carbon::now ()->subYear (); 
		$Users = User::approved ();
		$numUsers = count ( $Users );
		 
		$loans = Loan::select('book_id',DB::raw('count(*) as loan_count'))->where( 'created_at', '>', $yearAgo )->groupBy('book_id')->take($showMostBorrowedValue)->get();
		
		// sorting the loan collection by ammount times borrowed
		// since this collection is sorted in ascending order
		// by default we reverse the collection to get the correct list
		$loans = $loans->sortBy('loan_count')->reverse();
		
		return View::make ( 'member/home' )->with ( 'userloans', $loans )->with('reviews', $reviews)->with ( 'yearAgo', $yearAgo )->with('numUsers', $numUsers);
	}
}