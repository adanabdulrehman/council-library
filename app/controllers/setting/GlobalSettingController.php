<?php
use settings\service\GlobalSettingService;
use settings\GlobalSetting;
class GlobalSettingController extends BaseController {
	private $GlobalSettingService;
	
	function __construct(GlobalSettingService $GlobalSettingService) {
		$this->GlobalSettingService = $GlobalSettingService;
	}
	
	/*
	 * method to get the settings and pass them to the global settings view
	 */
	public function getSettings() {
		return View::make ( 'globalSettings/globalSetting' )->with ( 'settings', $this->GlobalSettingService->getSettings () );
	}
	/*
	 * method to validate the user input create a globalsettings object and pass it to the service class for updating the db
	 */
	public function changeSettings() {
		
		/* create a rules array */
		$rules = array (
				'Daily_Loan_Rate_in_Cents' => 'required|integer',
				'First_Year_Book_Allowance' => 'required|numeric|min:4',
				'Book_Allowance_After_1yr' => 'required|numeric|min:6',
				'Permissable_Loan_Days' => 'required|integer',
				'Show_Most_Borrowed' => 'required|integer|min:2|max:7', 
				'Show_Highest_Rated' => 'required|integer|min:2|max:7' 
		);
		
		$validator = Validator::make ( Input::all (), $rules );
		
		// process the global settings passed from the form in the view.
		if ($validator->fails ()) {
			return Redirect::to ( 'globalSettings' )->withErrors ( $validator )->withInput ( Input::all () );
		} else {
			
			$inputs = Input::all ();
			$firstYearBookAllowance = $inputs ['First_Year_Book_Allowance'];
			$secondYearBookAllowance = $inputs ['Book_Allowance_After_1yr'];
			
			if ($firstYearBookAllowance >=  $secondYearBookAllowance) {
				// add validation for checking that the First_Year_Book_Allowance < Book_Allowance_After_1yr
				return Redirect::to ( 'globalSettings' )->with ( 'message', "first year book allowance has to be less than book allowance after one year" );
			}
			
			$globalSetting = new GlobalSetting ();
			
			$globalSetting->setLoanRate ( $inputs ['Daily_Loan_Rate_in_Cents'] );
			$globalSetting->setGlobalBookAllowanceUnder1yr ( $inputs ['First_Year_Book_Allowance'] );
			$globalSetting->setGlobalBookAllowanceOver1yr ( $inputs ['Book_Allowance_After_1yr'] );
			$globalSetting->setPermissableLoanPeriod ( $inputs ['Permissable_Loan_Days'] );
			$globalSetting->setShowMostBorrowed( $inputs ['Show_Most_Borrowed'] );
			$globalSetting->setShowHighestRated ( $inputs ['Show_Highest_Rated'] );
			
			//pass the new object to the service class for db update
			$this->GlobalSettingService->setSettings ( $globalSetting );
			return Redirect::to ( 'globalSettings' )->with ( 'message', "Settings Changed" );
		}
	}
}