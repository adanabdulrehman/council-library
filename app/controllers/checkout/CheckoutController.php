<?php

// we are going to use Carbon dates so we include it
// this is resident in the vendor folder
use Carbon\Carbon;

// using the registered service for getting global settings
// for the application such as yearly bookAllowance, loan period etc
use settings\service\GlobalSettingService;
use settings\GlobalSetting;

// controller responsible for handling the lending
// or checking out of book items
class CheckoutController extends BaseController {
	public $restful = true;
	
	// Settings service to query the settings table
	private $GlobalSettingService;
	
	function __construct(GlobalSettingService $GlobalSettingService) {
		// inject the service
		$this->GlobalSettingService = $GlobalSettingService;
		
		// authenticate all actions based on a custom filter
		$this->beforeFilter ( 'librarian', array (
				'except' => array () 
		) );
		
		// protecting any post actions from csrf attacks
		$this->beforeFilter ( 'csrf', array (
				'on' => 'post' 
		) );
	}
	
	// post action to get a member
	public function doCheckoutGetMember() {
		
		// get the form inputs
		$inputs = Input::all ();
		$memberId = $inputs ['memberId'];
		
		// find the particular member
		$user = User::find ( $memberId );
		
		// make sure they are present and are members
		if (count ( $user ) && $user->hasRole ( 'member' )) {
			$booksAllowed = $user->book_allowance;
			// get the item loans for this user and do it old school style
			// for demo purposes of demonstrating mixing styles
			$userloans = $user->loans;
			$userloanCount = 0;
			
			foreach ( $userloans as $loan ) {
				if ($loan->status != 'returned') {
					$userloanCount ++;
				}
			}
			
			// make the view showing user details and loans
			return View::make ( 'Item/loan' )->with ( 'user', $user )->with ( 'count', $userloanCount )->with ( 'userloans', $userloans )->with ( 'memberId', $memberId );
		} else {
			$message = "user does not exist";
			return View::make ( 'Item/doLoan' )->with ( 'message', $message );
		}
	}
	
	// post action to do a book lend or checkout
	public function doBookCheckout() {
		
		// get the form inputs
		$inputs = Input::all ();
		$bookId = $inputs ['book_id'];
		$memberId = $inputs ['memberId'];
		
		$book = Book::find ( $bookId );
		
		// some processing will happen here and the result is that the item will be lent
		// find the particular user
		$user = User::find ( $memberId );
		
		// make sure they are present and are members
		if (count ( $user ) && $user->hasRole ( 'member' )) {
			$booksAllowed = $user->book_allowance;
			$userloans = $user->loans;
			$userloanCount = 0;
			
			// get the number of books on loan to member
			foreach ( $userloans as $loan ) {
				if ($loan->status != 'returned') {
					$userloanCount ++;
				}
			}
			
			if ($booksAllowed > $userloanCount) {
				if (count ( $book )) {
					if ($book->available) {
						$this->addLoan ( $book->id, $user->id );
						
						// This is an excellent place to update the book allowance limit
						// The reason is because at checkout the update takes effect
						// for the first time for the user's first borrowed book after 1 year
						$this->updateBookAllowanceLimit( $user->id );
						
						return $this->reloadDoBookCheckout ( $book->id, $user->id );
						
					} else {
						$message = "Book with id " . $bookId . " is not available and on loan";
					}
				} else {
					$message = "Book with id " . $bookId . " was not found";
				}
			} else {
				$message = "This member has reached there book allowance limit";
			}
			
			// make the view showing user details and loans and result
			return View::make ( 'Item/loan' )->with ( 'user', $user )->with ( 'count', $userloanCount )->with ( 'userloans', $userloans )->with ( 'memberId', $memberId )->with ( 'message', $message );
		} else {
			$message = "user does not exist";
			return View::make ( 'Item/doLoan' )->with ( 'message', $message );
		}
	}
	
	// local method to add an entry into the loans table
	private function addLoan($bookId, $userId) {
		$book = Book::find ( $bookId );
		$user = User::find ( $userId );
		
		// make the book unavailable for lending
		$book->available = false;
		$book->update ();
		
		// add an entry into the loans table
		$loan = new Loan ();
		$loan->book_id = $book->id;
		$loan->user_id = $user->id;
		
		$loanDays = $this->GlobalSettingService->getSettings ()->getPermissableLoanPeriod ();
		$loan->due_date = Carbon::now ()->addDays ( $loanDays );
		
		$loan->status = 'onloan';
		$loan->reviewed = false;
		$loan->save ();
		return;
	}
	
	private function updateBookAllowanceLimit( $userId ){
		$user = User::find ( $userId );
	
		// check that the user is a member
		if ($user->hasRole ( 'member' )) {
			
			// get the allowances
			$bookAllowanceOverYear = $this->GlobalSettingService->getSettings()->getGlobalBookAllowanceOver1yr();
			$bookAllowanceUnderYear = $this->GlobalSettingService->getSettings()->getGlobalBookAllowanceUnder1yr();
			
			// check if they already have been updated
			// note: this also updates the member with any changes made
			// to the settings by the administrator - lazy initialization
			if ($user->book_allowance == $bookAllowanceOverYear) {
				return;
			}
			else {
			
				if ($user->outstanding_fine > 0) {
					return;
				}
	
				// if 12 months has passed or a year then update
				// otherwise set it to (maybe an updated) under1year allowance
				if($user->created_at->diffInDays() > 365) {
					$user->book_allowance = $bookAllowanceOverYear;
				}
				else {
					$user->book_allowance = $bookAllowanceUnderYear;
				}
				
				$user->update();
				
				return;
			}
		}
	}
	
	// reloads and prepares page after book checkout 
	private function reloadDoBookCheckout($bookId, $memberId) {
		$message = "Successfully loaned book with id " . $bookId;
		// find the particular member
		$user = User::find ( $memberId );
		
		// make sure they are present and are members
		if (count ( $user ) && $user->hasRole ( 'member' )) {
			$booksAllowed = $user->book_allowance;
			
			// get the item loans for this user and do it old school style
			// for demo purposes of demonstrating mixing styles in Laravel
			// and also to show alternatives to the numerous Laravel styles
			// already implemented
			$userloans = $user->loans;
			$userloanCount = 0;
			
			foreach ( $userloans as $loan ) {
				if ($loan->status != 'returned') {
					$userloanCount ++;
				}
			}
			
			// make the view showing user details and loans
			return View::make ( 'Item/loan' )->with ( 'user', $user )->with ( 'count', $userloanCount )->with ( 'userloans', $userloans )->with ( 'memberId', $memberId )->with ( 'message', $message );
		} else {
			$message = "user does not exist";
			return View::make ( 'Item/doLoan' )->with ( 'message', $message );
		}
	}
	
	// shows the initial book lend view to get member
	public function showBookCheckout() {
		return View::make ( 'Item/doLoan' );
	}
}