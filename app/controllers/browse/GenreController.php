<?php

class GenreController extends \BaseController {

	// add a filter for authorization only allowing index and show resource
	function __construct() {
		$this->beforeFilter ( 'administrator', array ('except' => array ('index', 'show')) );
		
		// protecting any post actions from csrf attacks
		$this->beforeFilter('csrf', array('on'=>'post'));
	}
	
	
	// Display a listing of the genres.
	// @return Response
	public function index() {
		
		$genres = Genre::all ();
		return View::make ( 'genre.index' )->with ( 'genres', $genres );
	}
	
	// Show the form for creating a new genre.
	// @return Response
	public function create() {
		
		$genre = new Genre;
		return View::make ('genre.create')->with ( 'genre', $genre );
	}
	
	// Store a newly created genre in storage.
	// @return Response
	public function store() {
	
		$genre=new Genre;
	
		$inputs = Input::all ();
		$genre->name = $inputs ['name'];
		$genre->description = $inputs ['description'];
		$genre->save();
		return Redirect::route ('genre.index');
	}
	
	// Display the specified genre.
	// @param int $id
	// @return Response
	public function show($id) {
		$genre = Genre::find ( $id );
		$books = $genre->books;
		
		$message = Session::get('message', '');
	
		return View::make ('genre.show')->with ('genre', $genre)->with ('books', $books)->with('message', $message);
	}
	
	// Show the form for editing the specified genre.
	// @param int $id
	// @return Response
	public function edit($id) {
		$genre = Genre::find ( $id );
		return View::make ( 'genre.edit' )->with ( 'genre', $genre );
	}
	
	// Update the specified genre in storage.
	// @param int $id
	// @return Response
	public function update($id) {
	
		$genre = Genre::find ( $id );
		$inputs = Input::all ();
		$genre->name = $inputs ['name'];
		$genre->description = $inputs ['description'];
		$genre->update ();
	
		return Redirect::route ('genre.show', array($id))->with ('message', 'Genre updated.');
	}
	
	// Remove the specified genre from storage.
	// @param int $id
	// @return Response
	public function destroy($id) {
		$genre = Genre::find($id);
		$genre->delete();
	}
}
