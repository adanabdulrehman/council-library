<?php
class approveMemberController extends \BaseController {
	function __construct() {
		
		// authenticate all actions except index and show
		$this->beforeFilter ( 'librarian', array (
				'except' => array () 
		) );
		
		// protecting any post actions from csrf attacks
		$this->beforeFilter ( 'csrf', array (
				'on' => 'post' 
		) );
	}
	
	// @return Response
	public function index() {
		$unconfirmedMembers = User::pendingApproval ()->orderBy ( 'created_at' )->get ();
		
		$numberOfMembers = 0;
		
		foreach ( $unconfirmedMembers as $member ) {
			if ($member->hasRole ( 'member' )) {
				$numberOfMembers ++;
			}
		}
		return View::make ( 'approve.index' )->with ( 'users', $unconfirmedMembers )->with ( 'numberOfMembers', $numberOfMembers );
	}
	public function postUpdate() {
		$uid = Input::get ( 'user_id' );
		
		// checking which submit was clicked on
		if (Input::get ( 'Approve' )) {
			$user = User::find ( $uid );
			$user->status = 'complete';
			$user->update ();
			
			return Redirect::route ( 'approval.index' );
		} else if (Input::get ( 'Decline' )) {
			// if declined then use this method
			$this->postDecline ( $uid );
			return;
		}
	}
	
	// approved, they become a full member etc..
	public function postApprove($uid) {
		$this->update ( $uid );
		$unconfirmedMembers = User::pendingApproval ()->orderBy ( 'created_at' )->get ();
		
		return View::make ( 'approve.index' )->with ( 'users', $unconfirmedMembers );
	}
	
	// declined, deletes unwanted member and emails them
	public function postDecline($uid) {
		
		// send an email but you need to set up the email server
		// since this was developed locally an email server
		// could not be configured. 
		// Mail::pretend ();
		$data = array();
		
		// initialize the data to send like a specific message here
		
		/*
		 // This mail sending code needs to be uncommented and a sender address included
		 // also the mail server needs to be setup. It has been tested and works
		Mail::send ( 'emails.registrationFailure', $data, function ($message) {
			$message->to ( 'foo@example.com', 'John Smith' )->subject ( 'Welcome!' );
		} );
		
		*/
		$this->destroy ( $uid );
		return Redirect::route ( 'approval.index' );
	}
	
	// Remove the specified resource from storage.
	// @param int $id
	// @return Response
	public function destroy($id) {
		$user = User::find ( $id );
		$user->delete ();
	}
}
