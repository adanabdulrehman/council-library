<?php
//for visibility of library
namespace settings;

use Illuminate\Support\ServiceProvider;

//class to bind the interfaces to the implementations
class AppBindingsServiceProvider extends ServiceProvider {
	/**
	 * Register the binding
	 *
	 * @return void
	 *
	 */
	// this method enables dependency injection
	public function register() {
		$this->app->bind ( 'settings\service\GlobalSettingService', 'settings\service\impl\GlobalSettingServiceImpl' );
		$this->app->bind ( 'settings\repository\GlobalSettingRepository', 'settings\repository\impl\DbGlobalSettingRepository' );
	}
}