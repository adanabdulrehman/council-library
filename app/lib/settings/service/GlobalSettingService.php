<?php

namespace settings\service;

// a hook for the framework
interface GlobalSettingService {
	
	public function setSettings($globalSetting);
	public function getSettings();
}
