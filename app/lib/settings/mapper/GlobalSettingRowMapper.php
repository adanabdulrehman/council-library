<?php

namespace settings\mapper;

// uses the globalSetting class
 use settings\GlobalSetting; 
 
// implements rowmapper interface 
class GlobalSettingRowMapper implements RowMapper {
	
	// maps a db row to the GlobalSetting's instance variables
	function mapRow($row) {
		$globalSetting = new GlobalSetting();
		$globalSetting->setId($row->id);
		$globalSetting->setPermissableLoanPeriod ( $row->permissable_loan_period);
		$globalSetting->setLoanRate ( $row->loan_rate);
		$globalSetting->setGlobalBookAllowanceUnder1yr ( $row->global_book_allowance_under1yr );
		$globalSetting->setGlobalBookAllowanceOver1yr ( $row->global_book_allowance_over1yr );
		$globalSetting->setShowMostBorrowed ( $row->show_most_borrowed );
		$globalSetting->setShowHighestRated ( $row->show_highest_rated );
		return $globalSetting;
	}
	
	// superflous method for mapping to an array.
	function mapRowToArray($rows) {
		
		$globalSettings = array ();
		foreach ( $rows as $row ) {
			$globalSetting = $this->mapRow( $row );
			$globalSettings[] = $globalSetting;
		}
		return $globalSettings;
	}
	
	// maps a db row to a GlobalSetting object.
	function mapRowToObject($rows){
		foreach ( $rows as $row ) {
			$globalSetting = $this->mapRow( $row );
			return $globalSetting;
		}
	}
}