@extends('layouts.login.main')
	@section('header')
		USER REGISTRATION
	@stop
	
	@section('leftMenu')
	@parent
	@stop
	
@section('content')
{{ Form::open(array('url'=>'users/create', 'method' => 'post')) }}
    <h1>Please Register by filling all fields below</h1>
 
    <ul>
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
 	<span>&nbsp;</span>
	</h1>
    <label><span>User name :</span>{{ Form::text('username') }}</label>
    <label><span>Email address :</span>{{ Form::text('email') }}</label>
    <label><span>Password :</span>{{ Form::password('password') }}</label>
    <label><span>Confirm password :</span>{{ Form::password('password_confirmation') }}</label>
    <br>
    <label><span>First name :</span>{{ Form::text('firstname') }}</label>
    <label><span>Last name :</span>{{ Form::text('secondname') }}</label>
    <label><span>Address :</span>{{ Form::text('address') }}</label>
    <label><span>City :</span>{{ Form::text('city') }}</label>
    <label><span>Phone :</span>{{ Form::text('phone') }}</label>
 
    <label><span>&nbsp;</span>{{ Form::submit('Register')}}</label>

{{ Form::close() }}
@stop
