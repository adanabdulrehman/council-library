@extends('layouts.member.main')

	@section('header')
		BOOK - {{{$book->title}}}
	@stop
	
	@section('leftMenu')
	@parent
	@stop
	
	@section('content')
	
		{{{ isset($message) ? $message : '' }}}
		<br>Title    : {{{$book->title}}}
	    <br>Author   : <a href="{{{URL::to('author')}}}/{{{$book->author->id}}}">{{{$book->author->name}}}</a>
		<br>Isbn     : {{{$book->isbn}}}
		<br>Published: {{{$book->publication_date}}}
		<br>Category : <a href="{{{URL::to('genre')}}}/{{{$book->genre->id}}}">{{{$book->genre->name}}}</a>
		<br>Available: @if ($book->available === "1")
						    Yes
						@else
						    No
						@endif
		
	    <br><br>Reviews for:  {{{$book->title}}}<br><br>
	    
	    <ul>
	    @if(count($reviews))
	    <br>Total average rating: {{{$averageScore}}} / 5 <br>
	    @if($averageScore < 1)
	    	{{ HTML::image('images/0star.jpg', $alt="Score Zero", $attributes = array()) }}
	    	@elseif ($averageScore >= 1 && $averageScore < 2)
			    {{ HTML::image('images/1star.jpg', $alt="Score One", $attributes = array()) }}
			    @elseif ($averageScore >= 2 && $averageScore < 3)
				    {{ HTML::image('images/2star.jpg', $alt="Score Two", $attributes = array()) }}
				    @elseif ($averageScore >= 3 && $averageScore < 4)
					    {{ HTML::image('images/3star.jpg', $alt="Score Three", $attributes = array()) }}
					    @elseif ($averageScore >= 4 && $averageScore < 5)
						    {{ HTML::image('images/4star.jpg', $alt="Score Four", $attributes = array()) }}
						    @elseif ($averageScore >= 5)
							    {{ HTML::image('images/5star.jpg', $alt="Score Five", $attributes = array()) }}
							   
	    @endif
	    <br><br>All reviews <br><br>
	    	@foreach($reviews as $review)
	    			<li><b>Review Id:</b>{{{$review->id}}} </li>
	    			<li><b>Reviewed By:</b>{{{$review->user->username}}} </li>
					<li><b>Rating:</b> score given {{{$review->rating}}} out of 5<br>
					@if($review->rating < 1)
				    	{{ HTML::image('images/0star.jpg', $alt="Score Zero", $attributes = array()) }}
				    	@elseif ($review->rating >= 1 && $review->rating < 2)
						    {{ HTML::image('images/1star.jpg', $alt="Score One", $attributes = array()) }}
						    @elseif ($review->rating >= 2 && $review->rating < 3)
							    {{ HTML::image('images/2star.jpg', $alt="Score Two", $attributes = array()) }}
							    @elseif ($review->rating >= 3 && $review->rating < 4)
								    {{ HTML::image('images/3star.jpg', $alt="Score Three", $attributes = array()) }}
								    @elseif ($review->rating >= 4 && $review->rating < 5)
									    {{ HTML::image('images/4star.jpg', $alt="Score Four", $attributes = array()) }}
									    @elseif ($review->rating >= 5)
										    {{ HTML::image('images/5star.jpg', $alt="Score Five", $attributes = array()) }}
			@endif
					</li>
					<li><b>Comment:</b> {{{$review->comment}}}</li>
					<br>
		  	@endforeach
		  	
		@else
		   <br>There are no reviews for this book yet
		@endif
		</ul>
	<p>{{ $reviews->links() }}</p>
	@stop
