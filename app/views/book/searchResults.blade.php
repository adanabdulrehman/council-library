@extends('layouts.member.main') 
@section('header') 

LIST OF BOOKS FROM SEARCH
@stop

@section('leftMenu') 

@parent 
@stop 

@section('content') 
	@if(count($books) < 1)
		<p>No books found</p>

	@else
		<p>{{{count($books)}}} books found</p>
		<br>
		<p>Click on the Author to see details and all of the author's books</p>
		<p>Click on the Category to see details and all books in that category</p>

	<br>
	<section class="booklist">
	<br>{{{ isset($message) ? $message : '' }}} <br>
	<table>
		<thead>
			<tr>
				<td>Id</td>
				<td>Title</td>
				<td>ISBN</td>
				<td>Published</td>
				<td>Author</td>
				<td>Category</td>
				<td>Available</td>
			</tr>
		</thead>

		<tbody>
			@foreach($books as $book)
			<tr>
				<td>{{{$book->id}}}</td>
				<td><a href="{{{URL::to('book')}}}/{{{$book->id}}}">{{{$book->title}}}</a></td>
				<td>{{{$book->isbn}}}</td>
				<td>{{{$book->publication_date}}}</td>
				<td><a href="{{{URL::to('author')}}}/{{{$book->author->id}}}">{{{$book->author->name}}}</a></td>
				<td><a href="{{{URL::to('genre')}}}/{{{$book->genre->id}}}">{{{$book->genre->name}}}</a></td>
				@if ($book->available === "1") 
					<td>Yes</td> 
				@else
					<td>No</td> 
				@endif
				</tr>
			@endforeach
		</tbody>

	</table>
	</section>
	<br>
	<br>
	<p>{{ $books->links() }}</p>
	
	@endif 
@stop
