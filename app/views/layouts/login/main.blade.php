<!DOCTYPE html>

<head>
    <!--  stylesheets
          reset.css wipes out the default stylesheet
          styles.css is the main styles sheet -->
          
	<link rel="stylesheet" type="text/css" href="{{{URL::to('/')}}}/css/reset.css">
	<link rel="stylesheet" type="text/css" href="{{{URL::to('/')}}}/css/styles.css">
	
	<!-- javascripts
	     html5.js solves a bug for browsers less than IE 9
	     we are going for similar styles to the google colours-->
	     
	<!--[if lt IE 9]><script src="{{{URL::to('/')}}}/scripts/html5.js"></script><![endif]-->
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
	<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
	<script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
	
	<!--  date picker script -->
	<script>
	    $(function() {
	        $( "#datepicker" ).datepicker();
	    });
	    </script>
	<!-- alert fade script -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js">
	</script>
	
	<script>
		$(document).ready(function(){
		  $("#Content").mouseenter(function(){
		    $("#alertDiv").fadeOut(3000);
		    $("#errorDiv").fadeOut(6000);
		  });
		});
	</script>
	
	<title>TOWN COUNCIL BOOK LIBRARY</title>

</head>

<body>
	<header>
		<h1>TOWN COUNCIL BOOK LIBRARY - LOGIN PANEL</h1>
	</header>

	<!--  Top left navigation - login -->
	<nav class="top_menu">
	</nav>


	<!--  Left handside navigation menu links -->
	@section('leftMenu')
	<nav class="left_menu">
		<ul class="operations">
		<br>
			@if(!Auth::check())
				<li>{{ HTML::link('users/login', 'Login') }}</li>
				<li>{{ HTML::link('users/register', 'Register') }}</li>  
                <li><a href="{{URL::to('/')}}">Library</a> <li><br>
               	@else
                    <li>{{ HTML::link('users/logout', 'logout') }}</li>
                    <li><a href="{{URL::to('/')}}">Library</a> <li><br>
            @endif
		</ul>
	</nav>
	@show
	
	<section class="header">
		<br>
		<h2>@yield('header')</h2>
		<br>
	</section>

	<section id="Content" class="content">
	<section class="book_edit_form">
	       	 @yield('content')    	
   	</section> 

	<!-- messages can be sent to screen using this next if statement.
		 for example if you add something like this in a redirect:
		return Redirect::to('globalSettings')->with('message',"Settings Changed");
		the  message will fade out after the user is updated a script in the header 
		of the page handles this.
		 -->
		@if (Session::has('message'))
		<div id="alertDiv" class="alert-info">{{ Session::get('message') }}</div>
	@endif 
	<!-- Validation Errors will be picked up by this next if statement
		 for example if you add something like this in a redirect:
		 return Redirect::to( 'globalSettings' )->withErrors($validator);
		the  message will fade out after the user is updated a script in the header 
		of the page handles this.
		 -->
	@if($errors->has())
		<div id="errorDiv">
	   	@foreach ($errors->all() as $error)
	      	{{ $error }}</br>
	  	@endforeach
  		</div>
	@endif
	</section>

</body>
</html>