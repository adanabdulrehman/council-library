<!DOCTYPE html>

<head>
    <!--  stylesheets
          reset.css wipes out the default stylesheet
          styles.css is the main styles sheet -->
          
	<link rel="stylesheet" type="text/css" href="{{{URL::to('/')}}}/css/reset.css">
	<link rel="stylesheet" type="text/css" href="{{{URL::to('/')}}}/css/styles.css">
	
	<!-- javascripts
	     html5.js solves a bug for browsers less than IE 9
	     we are going for similar styles to the google colours-->
	     
	<!--[if lt IE 9]><script src="{{{URL::to('/')}}}/scripts/html5.js"></script><![endif]-->
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
	<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
	<script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
	
	<!--  date picker script -->
	<script>
	    $(function() {
	        $( "#datepicker" ).datepicker();
	    });
	    </script>
	<!-- alert fade script -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js">
	</script>
	
	<script>
		$(document).ready(function(){
		  $("#Content").mouseenter(function(){
		    $("#alertDiv").fadeOut(3000);
		    $("#errorDiv").fadeOut(6000);
		  });
		});
	</script>
	
	<title>TOWN COUNCIL BOOK LIBRARY</title>

</head>

<body>
	<header>
		<h1>TOWN COUNCIL BOOK LIBRARY - ADMINISTRATOR PANEL</h1>
	</header>

	<!--  Left handside navigation menu links -->
	@section('leftMenu')
	<nav class="left_menu">
	    <br>
		<ul class="operations">
			<li><a href="{{URL::to('/administrator')}}">Home</a> <li><br>
			<li><a href="{{URL::to('/book')}}">Library</a><li> 
			@if(Auth::check())
			    @if(Auth::user()->hasRole('administrator'))
					<li><a href="{{URL::to('/globalSettings')}}">Edit settings</a><li>  
					<li><a href="{{URL::to('/adminReport')}}">Admin report</a><li>   
					<li>{{ HTML::link('users/logout', 'logout') }}</li>
				@else
					<li>{{ HTML::link('users/login', 'Login') }}</li>
				@endif
			@else
				<li>{{ HTML::link('users/login', 'Login') }}</li>
       	 	@endif
       	 	
		</ul>
	</nav>
	@show

	<section class="header">
		<br><h2>@yield('header')</h2><br>
	</section>


	<section id="Content" class="content">
	@yield('content')
	<!-- messages can be sent to screen using this next if statement.
		 for example if you add something like this in a redirect:
		return Redirect::to('globalSettings')->with('message',"Settings Changed");
		the  message will fade out after the user is updated a script in the header 
		of the page handles this.
		 -->
		@if (Session::has('message'))
		<div id="alertDiv" class="alert-info">{{ Session::get('message') }}</div>
	@endif 
	<!-- Validation Errors will be picked up by this next if statement
		 for example if you add something like this in a redirect:
		 return Redirect::to( 'globalSettings' )->withErrors($validator);
		the  message will fade out after the user is updated a script in the header 
		of the page handles this.
		 -->
	@if($errors->has())
		<div id="errorDiv">
	   	@foreach ($errors->all() as $error)
	      	{{ $error }}</br>
	  	@endforeach
  		</div>
	@endif
	</section>

</body>
</html>