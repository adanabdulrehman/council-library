@extends('layouts.administrator.main')
<!-- this class contains a form for updating the various global settings
...thats it really  -->
	@section('header')
		EDIT GLOBAL MEMBER SETTINGS 
	@stop
	
	@section('leftMenu')
	@parent 
	@stop
	
	@section('content')
	<section class="globalSettings_edit_form">
	{{Form::open(array('url' => '/refreshGlobalSettings', 'method' =>
	'post')) }}
	<span>&nbsp;</span>
	{{{ isset($message) ? $message : '' }}}
	
	<label><span>Daily Loan Rate in Cents :</span>{{Form::text('Daily_Loan_Rate_in_Cents',$settings->getLoanRate())}}</label>
	<label><span>First Year Book Allowance:</span>{{Form::text('First_Year_Book_Allowance',$settings->getGlobalBookAllowanceUnder1yr())}}</label>
	<label><span>Book Allowance After 1yr :</span>{{Form::text('Book_Allowance_After_1yr',$settings->getGlobalBookAllowanceOver1yr())}}</label>
	<label><span>Permissable Loan Days  :</span>{{Form::text('Permissable_Loan_Days',$settings->getPermissableLoanPeriod())}}</label>
	<label><span>Borrowed Books Display:</span>{{Form::text('Show_Most_Borrowed',$settings->getShowMostBorrowed())}}</label>
	<label><span>Rated Books Display :</span>{{Form::text('Show_Highest_Rated',$settings->getShowHighestRated())}}</label>
	
	<label>{{Form::submit('Change')}}</label>
	{{Form::close()}}
</section>
@stop
