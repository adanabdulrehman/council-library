@extends('layouts.member.main')

	@section('header')
		CATEGORY - {{{$genre->name}}}
	@stop
	
	@section('leftMenu')
	@parent
	@stop
	
	@section('content')
		
		<br>Description:
		<br>{{{$genre->description}}}
	    <br><br>All books in this category<br><br>
	    
	    <ul>
	    	@foreach($books as $abook)
	    		<li><a href="{{{URL::to('book')}}}/{{{$abook->id}}}">{{{$abook->title}}}</a></li>
		  	@endforeach
		</ul>

	@stop