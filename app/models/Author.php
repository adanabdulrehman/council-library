<?php

// author model
class Author extends Eloquent {
	
	// An author will have many books
	public function books() {
		return $this->hasMany ('Book');
	}
}